package main

import (
	"cppcloud"
	"cppcloud/provider"
	"fmt"
	"net/http"
)

// 使用go sdk创建分布式服务提供者示例

func main() {
	appAttr := make(map[string]interface{})
	appAttr["svrid"] = 219
	appAttr["svrname"] = "HttpPrvd"
	capp := cppcloud.CreateCppCloudApp("cppcloud.cn:4800", appAttr, 3) // 创建sdk主对象
	capp.Start()

	prvd := provider.CreateHTTPProvider(capp, "", 2046)
	prvd.Start()

	httpServer := &MyHttpServer{}
	http.ListenAndServe(":2046", httpServer)
	capp.Shutdown()
}

type MyHttpServer struct {
}

func (*MyHttpServer) ServeHTTP(writer http.ResponseWriter, req *http.Request) {
	fmt.Println("recv http request")
	writer.Write([]byte("welcome"))
}
